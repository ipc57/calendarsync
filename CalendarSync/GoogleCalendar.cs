﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Timers;

namespace CalendarSync
{
    /// <summary>
    /// 6XooF91f3a
    /// </summary>
    class GoogleCalendar
    {
        private static GoogleCalendar instance;

        public static GoogleCalendar Instance
        {
            get
            {
                if (instance == null) instance = new GoogleCalendar();
                return instance;
            }
        }

        CalendarService service;

        public GoogleCalendar()
        {
            System.Net.WebProxy wp = (System.Net.WebProxy)System.Net.GlobalProxySelection.Select;
            wp.UseDefaultCredentials = true;
            System.Net.WebRequest.DefaultWebProxy = wp;

            UserCredential credential;
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                         new ClientSecrets { ClientId = "442753510144-5g0skd3cbhr80t99d44f4c53nkek3lci.apps.googleusercontent.com", ClientSecret = "FhP3S6C2CdYTlIRp75TuPqWL" },
                         new[] { CalendarService.Scope.Calendar },
                         "user",
                         CancellationToken.None,
                         new Google.Apis.Util.Store.FileDataStore("CalendarSync")).Result;

            service = new CalendarService(new BaseClientService.Initializer()
            {

                HttpClientInitializer = credential,
                ApplicationName = "CalSync",
            }
                 );


            //System.Diagnostics.Process.Start("https://accounts.google.com/o/oauth2/auth?redirect_uri=urn:ietf:wg:oauth:2.0:oob&response_type=code&client_id=442753510144-5g0skd3cbhr80t99d44f4c53nkek3lci.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&approval_prompt=force&access_type=offline");

            //System.Windows.Forms.Form code = new System.Windows.Forms.Form();
            //code.ShowDialog();

            //string queryStringFormat = @"code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code";
            //string postcontents = string.Format(queryStringFormat
            //                                    , HttpUtility.UrlEncode(code)
            //                                    , "442753510144-5g0skd3cbhr80t99d44f4c53nkek3lci.apps.googleusercontent.com"
            //                                    , "FhP3S6C2CdYTlIRp75TuPqWL"
            //                                    , "urn:ietf:wg:oauth:2.0:oob");
            //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://accounts.google.com/o/oauth2/token");
            //request.Method = "POST";
            //byte[] postcontentsArray = Encoding.UTF8.GetBytes(postcontents);
            //request.ContentType = "application/x-www-form-urlencoded";
            //request.ContentLength = postcontentsArray.Length;
            //using (Stream requestStream = request.GetRequestStream())
            //{
            //    requestStream.Write(postcontentsArray, 0, postcontentsArray.Length);
            //    requestStream.Close();
            //    WebResponse response = request.GetResponse();
            //    using (Stream responseStream = response.GetResponseStream())
            //    using (StreamReader reader = new StreamReader(responseStream))
            //    {
            //        string responseFromServer = reader.ReadToEnd();
            //        reader.Close();
            //        responseStream.Close();
            //        response.Close();
            //        return SerializeToken(responseFromServer);
            //    }
            //}


        }


        public List<MyCalendarListEntry> getCalendars()
        {
            CalendarList request = null;
            try
            {
                request = service.CalendarList.List().Execute();
            }
            catch (System.Exception ex)
            {
                logboxout("Error in getCalendars: " + ex.Message);
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            if (request != null)
            {

                List<MyCalendarListEntry> result = new List<MyCalendarListEntry>();
                foreach (CalendarListEntry cle in request.Items)
                {
                    result.Add(new MyCalendarListEntry(cle));
                }
                return result;
            }
            return null;
        }



        public List<Event> getCalendarEntriesInRange()
        {
            List<Event> result = new List<Event>();
            Events request = null;
            String pageToken = null;

            try
            {
                do
                {
                    EventsResource.ListRequest lr = service.Events.List(CalSettings.Instance.UseGoogleCalendar.Id);
                    lr.TimeMin = DateTime.Parse(GoogleTimeFrom(DateTime.Today.AddDays(-Convert.ToInt32(Properties.Settings.Default.SyncDaysPast))));
                    lr.TimeMax = DateTime.Parse(GoogleTimeFrom(DateTime.Today.AddDays(+Convert.ToInt32(Properties.Settings.Default.SyncDaysFuture) + 1)));
                    lr.PageToken = pageToken;
                    lr.SingleEvents = true;
                    request = lr.Execute();
                    pageToken = request.NextPageToken;
                     if (request != null)
                     {
                        if (request.Items != null) result.AddRange(request.Items);
                     }
                } while (pageToken != null);



            }
            catch (System.Exception ex)
            {
                logboxout("Error in getCalendarEntriesInRange: " + ex.Message);
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            //if (request != null)
            //{
            //    if (request.Items != null) result.AddRange(request.Items);
            //}
            return result;
        }

       

        public Event UpdateCalendarEntry(Event e, string Id)
        {
            Thread.Sleep(1500);
            Event request = null;
            try
            {
                request = service.Events.Update(e, CalSettings.Instance.UseGoogleCalendar.Id, Id).Execute();
               
            }
            catch (System.Exception ex)
            {
                logboxout("Error in UpdateCalendarEntry: " + ex.Message);
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            
            return request;
        }


        public string deleteCalendarEntry(string Id)
        {
            Thread.Sleep(1500);
            string request = "";

            try
            {
                request = service.Events.Delete(CalSettings.Instance.UseGoogleCalendar.Id, Id).Execute();
            }
            catch (System.Exception ex)
            {
                logboxout("Error in deleteCalendarEntry: " + ex.Message);
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            return request;
        }

        public Event addEntry(Event e)
        {
            Thread.Sleep(1500);
            Event result = null;

            try
            {
                result = service.Events.Insert(e, CalSettings.Instance.UseGoogleCalendar.Id).Execute();
            }
            catch (System.Exception ex)
            {
                logboxout("Error in addEntry: " + ex.Message);
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            return result;
        }

        public static string GetTimeZone(DateTime dt)
        {
            string timezone = TimeZoneInfo.Local.GetUtcOffset(dt).ToString();
            if (timezone[0] != '-') timezone = '+' + timezone;
            timezone = timezone.Substring(0, 6);

            return timezone;
        }

        //returns the Google Time Format String of a given .Net DateTime value
        //Google Time Format = "2012-08-20T00:00:00+02:00"
        //public static string GoogleTimeFrom(DateTime dt)
        //{
        //    string result = dt.GetDateTimeFormats('s')[0] + GetTimeZone(dt);
        //    return result;
        //}


        public static string GoogleTimeFrom(DateTime dt)
        {
            string timezone = TimeZoneInfo.Local.GetUtcOffset(dt).ToString();
            if(timezone[0] != '-') timezone = '+' + timezone;
            timezone = timezone.Substring(0, 6);

            string result = dt.GetDateTimeFormats('s')[0] + timezone;
            return result;
        }

        public static string signature(Event ev)
        {
            String signature = "";
            signature += (ev.Start.DateTime == null) ?
                GoogleTimeFrom(DateTime.Parse(ev.Start.Date)) :
                GoogleTimeFrom(DateTime.Parse(ev.Start.DateTime.ToString()));
            signature += ";" + ((ev.End.DateTime == null) ?
                GoogleTimeFrom(DateTime.Parse(ev.End.Date)) :
                GoogleTimeFrom(DateTime.Parse(ev.End.DateTime.ToString())));
            signature += ";" + ev.Summary + ";" + ev.Location;

            return signature.Trim();
        }

        void logboxout(string s)
        {
            StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CalendarSync\\SyncLog.log", true);
            sw.Write(DateTime.Now.ToString() + " - " + s + Environment.NewLine);
            sw.Close();
        }
    }
}
