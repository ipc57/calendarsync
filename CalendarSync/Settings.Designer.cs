﻿namespace CalendarSync
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_Calendars = new System.Windows.Forms.ComboBox();
            this.textBox_SyncDaysPast = new System.Windows.Forms.TextBox();
            this.textBox_SyncDaysFuture = new System.Windows.Forms.TextBox();
            this.textBox_SyncAutoInterval = new System.Windows.Forms.TextBox();
            this.checkBox_SyncAuto = new System.Windows.Forms.CheckBox();
            this.button_GetCalendars = new System.Windows.Forms.Button();
            this.button_save = new System.Windows.Forms.Button();
            this.label_SyncDaysPast = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label_VersionAvailable = new System.Windows.Forms.Label();
            this.label_VersionCurrent = new System.Windows.Forms.Label();
            this.button_update = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_loglevel = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox_Calendars
            // 
            this.comboBox_Calendars.FormattingEnabled = true;
            this.comboBox_Calendars.Location = new System.Drawing.Point(15, 38);
            this.comboBox_Calendars.Name = "comboBox_Calendars";
            this.comboBox_Calendars.Size = new System.Drawing.Size(224, 21);
            this.comboBox_Calendars.TabIndex = 0;
            // 
            // textBox_SyncDaysPast
            // 
            this.textBox_SyncDaysPast.Location = new System.Drawing.Point(151, 27);
            this.textBox_SyncDaysPast.Name = "textBox_SyncDaysPast";
            this.textBox_SyncDaysPast.Size = new System.Drawing.Size(36, 20);
            this.textBox_SyncDaysPast.TabIndex = 1;
            // 
            // textBox_SyncDaysFuture
            // 
            this.textBox_SyncDaysFuture.Location = new System.Drawing.Point(151, 49);
            this.textBox_SyncDaysFuture.Name = "textBox_SyncDaysFuture";
            this.textBox_SyncDaysFuture.Size = new System.Drawing.Size(36, 20);
            this.textBox_SyncDaysFuture.TabIndex = 1;
            // 
            // textBox_SyncAutoInterval
            // 
            this.textBox_SyncAutoInterval.Location = new System.Drawing.Point(57, 49);
            this.textBox_SyncAutoInterval.Name = "textBox_SyncAutoInterval";
            this.textBox_SyncAutoInterval.Size = new System.Drawing.Size(27, 20);
            this.textBox_SyncAutoInterval.TabIndex = 1;
            // 
            // checkBox_SyncAuto
            // 
            this.checkBox_SyncAuto.AutoSize = true;
            this.checkBox_SyncAuto.Location = new System.Drawing.Point(9, 26);
            this.checkBox_SyncAuto.Name = "checkBox_SyncAuto";
            this.checkBox_SyncAuto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox_SyncAuto.Size = new System.Drawing.Size(75, 17);
            this.checkBox_SyncAuto.TabIndex = 2;
            this.checkBox_SyncAuto.Text = "Auto Sync";
            this.checkBox_SyncAuto.UseVisualStyleBackColor = true;
            // 
            // button_GetCalendars
            // 
            this.button_GetCalendars.Location = new System.Drawing.Point(245, 24);
            this.button_GetCalendars.Name = "button_GetCalendars";
            this.button_GetCalendars.Size = new System.Drawing.Size(65, 47);
            this.button_GetCalendars.TabIndex = 3;
            this.button_GetCalendars.Text = "Get Calendars";
            this.button_GetCalendars.UseVisualStyleBackColor = true;
            this.button_GetCalendars.Click += new System.EventHandler(this.button_GetCalendars_Click);
            // 
            // button_save
            // 
            this.button_save.Location = new System.Drawing.Point(12, 192);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(316, 29);
            this.button_save.TabIndex = 4;
            this.button_save.Text = "Save and Exit";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // label_SyncDaysPast
            // 
            this.label_SyncDaysPast.AutoSize = true;
            this.label_SyncDaysPast.Location = new System.Drawing.Point(12, 30);
            this.label_SyncDaysPast.Name = "label_SyncDaysPast";
            this.label_SyncDaysPast.Size = new System.Drawing.Size(127, 13);
            this.label_SyncDaysPast.TabIndex = 5;
            this.label_SyncDaysPast.Text = "Synchronize days in past:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Synchzonize days in future:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Interval:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Choose a calendar";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_SyncAutoInterval);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.checkBox_SyncAuto);
            this.groupBox1.Location = new System.Drawing.Point(222, 99);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(106, 86);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "How";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_SyncDaysFuture);
            this.groupBox2.Controls.Add(this.textBox_SyncDaysPast);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label_SyncDaysPast);
            this.groupBox2.Location = new System.Drawing.Point(12, 99);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(204, 87);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "What";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboBox_Calendars);
            this.groupBox3.Controls.Add(this.button_GetCalendars);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(316, 81);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Calendar";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label_VersionAvailable);
            this.groupBox4.Controls.Add(this.label_VersionCurrent);
            this.groupBox4.Location = new System.Drawing.Point(334, 99);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(196, 86);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Update";
            // 
            // label_VersionAvailable
            // 
            this.label_VersionAvailable.AutoSize = true;
            this.label_VersionAvailable.Location = new System.Drawing.Point(6, 52);
            this.label_VersionAvailable.Name = "label_VersionAvailable";
            this.label_VersionAvailable.Size = new System.Drawing.Size(87, 13);
            this.label_VersionAvailable.TabIndex = 5;
            this.label_VersionAvailable.Text = "Available version";
            // 
            // label_VersionCurrent
            // 
            this.label_VersionCurrent.AutoSize = true;
            this.label_VersionCurrent.Location = new System.Drawing.Point(6, 30);
            this.label_VersionCurrent.Name = "label_VersionCurrent";
            this.label_VersionCurrent.Size = new System.Drawing.Size(78, 13);
            this.label_VersionCurrent.TabIndex = 5;
            this.label_VersionCurrent.Text = "Current version";
            // 
            // button_update
            // 
            this.button_update.Location = new System.Drawing.Point(334, 191);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(196, 30);
            this.button_update.TabIndex = 4;
            this.button_update.Text = "Update";
            this.button_update.UseVisualStyleBackColor = true;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.linkLabel1);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.comboBox_loglevel);
            this.groupBox5.Location = new System.Drawing.Point(334, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(196, 81);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Logging";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(6, 53);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(51, 13);
            this.linkLabel1.TabIndex = 2;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Show log";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "LogLevel (0=All, 1=Errors):";
            // 
            // comboBox_loglevel
            // 
            this.comboBox_loglevel.FormattingEnabled = true;
            this.comboBox_loglevel.Items.AddRange(new object[] {
            "0",
            "1",
            "2"});
            this.comboBox_loglevel.Location = new System.Drawing.Point(147, 19);
            this.comboBox_loglevel.Name = "comboBox_loglevel";
            this.comboBox_loglevel.Size = new System.Drawing.Size(43, 21);
            this.comboBox_loglevel.TabIndex = 0;
            this.comboBox_loglevel.Text = "0";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 233);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button_update);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button_save);
            this.Name = "Settings";
            this.Text = "CalendarSync settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_Calendars;
        private System.Windows.Forms.TextBox textBox_SyncDaysPast;
        private System.Windows.Forms.TextBox textBox_SyncDaysFuture;
        private System.Windows.Forms.TextBox textBox_SyncAutoInterval;
        private System.Windows.Forms.CheckBox checkBox_SyncAuto;
        private System.Windows.Forms.Button button_GetCalendars;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Label label_SyncDaysPast;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label_VersionCurrent;
        private System.Windows.Forms.Label label_VersionAvailable;
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_loglevel;
    }
}