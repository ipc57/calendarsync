﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CalendarSync
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            MyCalendarListEntry calendars = new MyCalendarListEntry();

            calendars.Name = Properties.Settings.Default.SelectedGoogleCalendar;
            calendars.Id = Properties.Settings.Default.SelectedGoogleCalendarId;
            comboBox_Calendars.Items.Add(calendars);
            comboBox_Calendars.SelectedIndex = 0;

            textBox_SyncDaysPast.Text = Properties.Settings.Default.SyncDaysPast;
            textBox_SyncDaysFuture.Text = Properties.Settings.Default.SyncDaysFuture;
            checkBox_SyncAuto.Checked = Properties.Settings.Default.SyncAutoEnabled;
            textBox_SyncAutoInterval.Text = Properties.Settings.Default.SyncAutoInterval;
            comboBox_loglevel.Text = Properties.Settings.Default.LogLevel.ToString();

            label_VersionCurrent.Text = "Aktuální verze: " + Properties.Settings.Default.VersionCurrent;
            label_VersionAvailable.Text = "Dostupná verze: " + Properties.Settings.Default.VersionAvailable;
            

        }


        private void Save()
        {
            MyCalendarListEntry calendars = (MyCalendarListEntry)comboBox_Calendars.SelectedItem;

            Properties.Settings.Default.SelectedGoogleCalendar = calendars.Name;
            Properties.Settings.Default.SelectedGoogleCalendarId = calendars.Id;
            Properties.Settings.Default.SyncDaysPast = textBox_SyncDaysPast.Text;
            Properties.Settings.Default.SyncDaysFuture = textBox_SyncDaysFuture.Text;
            Properties.Settings.Default.SyncAutoEnabled = checkBox_SyncAuto.Checked;
            Properties.Settings.Default.SyncAutoInterval = textBox_SyncAutoInterval.Text;
            Properties.Settings.Default.LogLevel = Convert.ToInt32(comboBox_loglevel.SelectedItem.ToString());    
            Properties.Settings.Default.Save();
        }



        private void button_GetCalendars_Click(object sender, EventArgs e)
        {
            button_GetCalendars.Enabled = false;
            comboBox_Calendars.Enabled = false;

            List<MyCalendarListEntry> calendars = GoogleCalendar.Instance.getCalendars();
            if (calendars != null)
            {
                comboBox_Calendars.Items.Clear();
                foreach (MyCalendarListEntry mcle in calendars)
                {
                    comboBox_Calendars.Items.Add(mcle);
                }
                comboBox_Calendars.SelectedIndex = 0;
            }

            button_GetCalendars.Enabled = true;
            comboBox_Calendars.Enabled = true;

            Properties.Settings.Default.IsNewCalendar = true;
            Properties.Settings.Default.Save();
        }

        private void checkBox_SyncAuto_CheckedChanged(object sender, EventArgs e)
        {
           // if (checkBox_SyncAuto.Checked) Globals.ThisAddIn.ogstimer.Enabled = true;
           // if (!checkBox_SyncAuto.Checked) Globals.ThisAddIn.ogstimer.Enabled = false;
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            if (checkBox_SyncAuto.Checked) Globals.ThisAddIn.SyncTimer.Enabled = true;
            if (!checkBox_SyncAuto.Checked) Globals.ThisAddIn.SyncTimer.Enabled = false;

            Save();
            this.Close();
        }

        private void button_update_Click(object sender, EventArgs e)
        {
            DoUpdate();
        }

        public static string[] installer32 { get; set; }

        public static string[] installer64 { get; set; }

        public static void DoUpdate()
        {
            Uri InstallerPath = new Uri("C:\\Program Files\\Common Files\\microsoft shared\\VSTO\\9.0\\VSTOINSTALLER.exe");

            if (Directory.Exists(@"C:\Program Files"))
            {
                try
                {
                    installer64 = System.IO.Directory.GetFiles(@"C:\Program Files\Common Files\Microsoft Shared\VSTO", "VSTOInstaller.exe", SearchOption.AllDirectories);
                    if (installer64[0].Length > 10) InstallerPath = new Uri(installer64[0]);
                }
                catch (System.Exception)
                { }
            }

            if (Directory.Exists(@"C:\Program Files (x86)"))
            {
                try
                {
                    installer32 = System.IO.Directory.GetFiles(@"C:\Program Files (x86)\Common Files\Microsoft Shared\VSTO", "VSTOInstaller.exe", SearchOption.AllDirectories);
                    if (installer32[0].Length > 10) InstallerPath = new Uri(installer32[0]);
                }
                catch (System.Exception)
                { }
            }

            try
            {
                Process VstoInstallerProc = new System.Diagnostics.Process();
                VstoInstallerProc.StartInfo.Arguments = " /I " + Properties.Settings.Default.DeployPath;
                VstoInstallerProc.StartInfo.FileName = InstallerPath.AbsoluteUri;
                VstoInstallerProc.Start();

                NotifyIcon ni0 = new NotifyIcon();
                ni0.Icon = SystemIcons.Information;
                ni0.Visible = true;
                ni0.ShowBalloonTip(3000, "CalendarSync Update", "Probíhá aktualizace. Prosím vyčkejte...", ToolTipIcon.Info);

                VstoInstallerProc.WaitForExit();
                ni0.Dispose();

                if (VstoInstallerProc.ExitCode == 0)
                {
                    NotifyIcon ni = new NotifyIcon();
                    ni.Icon = SystemIcons.Information;
                    ni.Visible = true;
                    ni.ShowBalloonTip(3000, "CalendarSync Update", "Aktualizace proběhla v pořádku. Naintalována verze " + Properties.Settings.Default.VersionAvailable + ". Restartujte prosím Outlook.", ToolTipIcon.Info);
                }
                else
                {
                    NotifyIcon ni = new NotifyIcon();
                    ni.Icon = SystemIcons.Error;
                    ni.Visible = true;
                    ni.ShowBalloonTip(3000, "CalendarSync Update", "Vyskytla se chyba při aktualizaci (" + VstoInstallerProc.ExitCode.ToString() + ")", ToolTipIcon.Error);
                }
            }
            catch (System.Exception)
            { }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string appFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CalendarSync";

            string logfilepath = appFolder + @"\SyncLog" + "." + DateTime.Now.ToString("dd.MM.yyyy") + ".log";

            if (!File.Exists(logfilepath))
            {
                MessageBox.Show("There is no log yet. Do some synchronization first.");
                return;
            }

            Process.Start(logfilepath);
        }
    }
}
