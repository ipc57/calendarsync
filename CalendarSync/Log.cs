﻿using CalendarSync.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkofitCopy
{
    class L
    {
        private static L singleton;
        public static L O
        {
            get { return singleton ?? (singleton = new L()); }
        }

        public void G(string text, int LogLevel)
        {
            string AppFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CalendarSync";

            if (Settings.Default.LogLevel > LogLevel) return;

            var sw = new System.IO.StreamWriter(AppFolder + @"\SyncLog" +  "." + DateTime.Now.ToString("dd.MM.yyyy") + ".log", true);
            sw.Write(DateTime.Now + " - " + text + Environment.NewLine);
            sw.Flush();
            sw.Close();
        }

        //public static class LogLevel
        //{
        //    public const int ALL = 0;
        //    public const int ERROR = 1;
        //    public const int NONE = 2;
        //}
    }
}
