﻿using System;
using System.Collections.Generic;


namespace CalendarSync
{

        public class EventArgs<T> : EventArgs
        {
            private T _value;

            public EventArgs(T aValue)
            {
                _value = aValue;
            }

            public T Value
            {
                get { return _value; }
                set { _value = value; }
            }
        }
 
}
