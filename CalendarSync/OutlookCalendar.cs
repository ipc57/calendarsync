﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.IO;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace CalendarSync
{
    class OutlookCalendar
    {
        private static OutlookCalendar instance;

        public static OutlookCalendar Instance
        {
            get
            {
                if (instance == null) instance = new OutlookCalendar();
                return instance;
            }
        }

        public Outlook.Folder UseOutlookCalendar;


        public OutlookCalendar()
        {
            UseOutlookCalendar = Globals.ThisAddIn.Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar) as Outlook.Folder;
        }


        public List<AppointmentItem> getCalendarEntries()
        {
            Items OutlookItems = UseOutlookCalendar.Items;
            if (OutlookItems != null)
            {
                List<AppointmentItem> result = new List<AppointmentItem>();
                foreach (AppointmentItem ai in OutlookItems)
                {
                    result.Add(ai);
                }
                return result;
            }
            return null;
        }


        public List<AppointmentItem> getCalendarEntriesInRange()
        {
            List<AppointmentItem> result = new List<AppointmentItem>();

            Items OutlookItems = UseOutlookCalendar.Items;
            OutlookItems.Sort("[Start]", Type.Missing);
            OutlookItems.IncludeRecurrences = true;

            if (OutlookItems != null)
            {
                DateTime min = DateTime.Today.AddDays(-Convert.ToInt32(Properties.Settings.Default.SyncDaysPast));
                DateTime max = DateTime.Today.AddDays(+Convert.ToInt32(Properties.Settings.Default.SyncDaysFuture) + 1);

                 
                string filter = "[End] >= '" + min.ToString("g") + "' AND [Start] < '" + max.ToString("g") + "'";
                logboxout("getCalendarEntriesInRange from Outlook - filter: " + filter);

                foreach (AppointmentItem ai in OutlookItems.Restrict(filter))
                {
                    result.Add(ai);
                }
            }
            return result;
        }

        public static string signature(AppointmentItem ai)
        {
            return (GoogleCalendar.GoogleTimeFrom(ai.Start) + ";" + GoogleCalendar.GoogleTimeFrom(ai.End) + ";" + ai.Subject + ";" + ai.Location).Trim();
        }

        void logboxout(string s)
        {
            StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CalendarSync\\SyncLog.log", true);
            sw.Write(DateTime.Now.ToString() + " - " + s + Environment.NewLine);
            sw.Close();
        }

    }
}
