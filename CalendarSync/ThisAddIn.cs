﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Google.Apis.Calendar.v3.Data;
using Microsoft.Office.Interop.Outlook;
using SkofitCopy;
using Exception = System.Exception;
using Timer = System.Timers.Timer;

namespace CalendarSync
{
    /// <summary>
    ///6XooF91f3a
    /// </summary>
    public partial class ThisAddIn
    {
        public const String PR_SMTP_ADDRESS = "http://schemas.microsoft.com/mapi/proptag/0x39FE001E";
        public List<int> MinuteOffsets = new List<int>();
        public Timer SyncTimer;
        public Timer SyncTimerExternal;
        public bool cbAddReminders = false;
        public bool IsSyncRunnig = false;
        public static string[] installer32 { get; set; }

        public static string[] installer64 { get; set; }

        private void ThisAddIn_Startup(object sender, EventArgs e)
        {
            //string AppFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CalendarSync";

            SyncTimerExternal = new Timer(1);
            SyncTimerExternal.Elapsed += SyncTimerExternal_Elapsed;
            SyncTimerExternal.Stop();

            SyncTimer = new Timer();
            SyncTimer.Interval = Convert.ToInt32(Properties.Settings.Default.SyncAutoInterval)*60000;
            SyncTimer.Elapsed += SyncTimer_Tick;
            SyncTimer.Enabled = Properties.Settings.Default.SyncAutoEnabled;

            GetVersionInfo();
        }
        public void DoSync()
        {
            SyncTimerExternal.Start();
        }
        void SyncTimerExternal_Elapsed(object sender, ElapsedEventArgs e)
        {
            var ni = new NotifyIcon();
            ni.Icon = SystemIcons.Question;
            ni.Visible = true;
            ni.ShowBalloonTip(3000, "CalendarSync", "Synchronizing...", ToolTipIcon.None);


           SyncTimerExternal.Stop();

           if (IsSyncRunnig)
           {
               MessageBox.Show("Another synchronization process already running. Wait a while.");
               return;
           }

            OnSyncButton();
            IsSyncRunnig = false;

           ni = new NotifyIcon();
           ni.Icon = SystemIcons.Question;
           ni.Visible = true;
           ni.ShowBalloonTip(3000, "CalendarSync", "Synchronizing finished.", ToolTipIcon.None);
        }

        private void SyncTimer_Tick(object sender, ElapsedEventArgs e)
        {
            OnSyncButton();
            IsSyncRunnig = false;
        }

        private void monitor_AppointmentAdded(object sender, EventArgs<AppointmentItem> e)
        {
            OnSyncButton();
        }

        private void monitor_AppointmentModified(object sender, EventArgs<AppointmentItem> e)
        {
            OnSyncButton();
        }

        private void monitor_AppointmentDeleting(object sender, CancelEventArgs<AppointmentItem> e)
        {
            OnSyncButton();
        }


        public void OnSyncButton()
        {
            if (IsSyncRunnig) return;
 
            CalSettings.Instance.UseGoogleCalendar.Id = Properties.Settings.Default.SelectedGoogleCalendarId;
            if (CalSettings.Instance.UseGoogleCalendar.Id == "")
            {
                MessageBox.Show(@"Nejprve v 'Nastavení' vyberte kalendář k synchronizaci.");
                return;
            }

            List<AppointmentItem> outlookEntries = OutlookCalendar.Instance.getCalendarEntriesInRange();
            List<Event> googleEntries = GoogleCalendar.Instance.getCalendarEntriesInRange();

            L.O.G("Outlook entries count: " + outlookEntries.Count.ToString(), 0);
            L.O.G("Google entries count: " + googleEntries.Count.ToString(), 0);

            var oSignatures = new List<string>();
            var gSignatures = new List<string>();

            if (outlookEntries.Count <= 0) return;

            //provádět kontrolu počtu vrácenách událostí pouze pokud kalendář nebyl nově nastaven a nejspíš je prázdný
            if (Properties.Settings.Default.IsNewCalendar == false)
            {
                if (googleEntries.Count <= 0) return;
            }

            Properties.Settings.Default.IsNewCalendar = false;
            Properties.Settings.Default.Save();

            IsSyncRunnig = true;

            foreach (AppointmentItem ai in outlookEntries)
            {
                string oSignature = OutlookCalendar.signature(ai);
                L.O.G("Outlook signature: " + oSignature,0);
                oSignatures.Add(oSignature);
            }

            foreach (Event ev in googleEntries)
            {
                string gSignature = GoogleCalendar.signature(ev);
                L.O.G("Google signature: " + gSignature,0);
                gSignatures.Add(gSignature);
            }

            //porovná dva seznamy a najde rozdíly - v tomto případě chybějící události v google a v outlooku
            List<string> notInGoogle = oSignatures.Except(gSignatures).ToList();
            List<string> notInOutlook = gSignatures.Except(oSignatures).ToList();

            L.O.G("For add to Google: " + notInGoogle.Count.ToString(), 0);
            L.O.G("For delete from Google: " + notInOutlook.Count.ToString(), 0);

            //přidá do Google to co v něm není
            foreach (string item in notInGoogle)
            {
                Thread.Sleep(300);
                //najde událost odpovídající signature a přidá ji do google
                foreach (AppointmentItem ai in outlookEntries)
                {
                    if (OutlookCalendar.signature(ai).Equals(item))
                    {
                        L.O.G("Adding to Google: " + item, 0);
                        Event ev = PrepareGoogleEvent(ai);
                        Event CreatedEvent = GoogleCalendar.Instance.addEntry(ev);
                    }
                }
            }

            //odebere z Google co není v Outlook
            foreach (string item in notInOutlook)
            {
                Thread.Sleep(300);
                //najde událost odpovídající signature a smaže ji v google. Jelikož znám pouze signature, musím tznovu projít všechny události a najít tu správnou
                foreach (Event ev in googleEntries)
                {
                    if (GoogleCalendar.signature(ev).Equals(item))
                    {
                        //smazat pouze pokud jsme ji sami vytvořili - nemazat uživatelem vytvořené události
                        if (ev.ExtendedProperties != null)
                        {
                            if (!ev.ExtendedProperties.Private.ContainsKey("outlook_EntryID")) continue;
                            L.O.G("Removing from Google: " + item, 0);
                            string result = GoogleCalendar.Instance.deleteCalendarEntry(ev.Id); 
                        }
 
                    }
                }
            }

            IsSyncRunnig = false;
        }

        private Event PrepareGoogleEvent(AppointmentItem ai)
        {
            CalSettings.Instance.UseGoogleCalendar.Id = Properties.Settings.Default.SelectedGoogleCalendarId;

            if (CalSettings.Instance.UseGoogleCalendar.Id == "")
            {
                MessageBox.Show(@"Nejprve v 'Nastavení' vyberte kalendář k synchronizaci.");
                //return;
            }

            //string GoogleId = GetUserProperty(ai,"GoogleId");
            //if (GoogleId.Length > 0) GoogleCalendar.Instance.deleteCalendarEntry(GoogleId);

            L.O.G("Converting Outlook appointment to Google event. " + ai.Subject,0);
            var ev = new Event();
            ev.Start = new EventDateTime();
            ev.End = new EventDateTime();

            if (ai.AllDayEvent)
            {
                ev.Start.Date = ai.Start.ToString("yyyy-MM-dd");
                ev.End.Date = ai.End.ToString("yyyy-MM-dd");
            }
            else
            {
                ev.Start.DateTime = DateTime.Parse(GoogleCalendar.GoogleTimeFrom(ai.Start));
                ev.End.DateTime = DateTime.Parse(GoogleCalendar.GoogleTimeFrom(ai.End));
            }
            ev.Summary = ai.Subject;
            ev.Description = ai.Body;
            ev.Location = ai.Location;

            ev.Visibility = (ai.Sensitivity == OlSensitivity.olNormal) ? "default" : "private";
            ev.Transparency = (ai.BusyStatus == OlBusyStatus.olFree) ? "transparent" : "opaque";

            var exp = new Event.ExtendedPropertiesData();
            exp.Private = new Dictionary<string, string>();

            if (ai.IsRecurring)
                exp.Private.Add("outlook_EntryID", ai.EntryID + "_" + ai.Start.ToString("yyyyMMdd"));
            else
                exp.Private.Add("outlook_EntryID", ai.EntryID);

            ev.ExtendedProperties = exp;

            if (ai.ResponseStatus == OlResponseStatus.olResponseAccepted) ev.Status = "confirmed";
            if (ai.ResponseStatus == OlResponseStatus.olResponseTentative) ev.Status = "tentative";
            if (ai.ResponseStatus == OlResponseStatus.olResponseNone) ev.Status = "tentative";
            if (ai.ResponseStatus == OlResponseStatus.olResponseNotResponded) ev.Status = "tentative";
            if (ai.ResponseStatus == OlResponseStatus.olResponseDeclined) ev.Status = "cancelled";

            var evo = new Event.OrganizerData();
            evo.DisplayName = ai.Organizer;
            ev.Organizer = evo;

            //consider the reminder set in Outlook
            if (cbAddReminders && ai.ReminderSet)
            {
                ev.Reminders = new Event.RemindersData();
                ev.Reminders.UseDefault = false;
                var reminder = new EventReminder();
                reminder.Method = "popup";
                reminder.Minutes = ai.ReminderMinutesBeforeStart;
                ev.Reminders.Overrides = new List<EventReminder>();
                ev.Reminders.Overrides.Add(reminder);
            }

            //Recipient as attendees
            if (ai.Recipients.Count > 0)
            {
                IList<EventAttendee> listOfAtendees = new List<EventAttendee>();
                foreach (Recipient recipient in ai.Recipients)
                {
                    var ea = new EventAttendee();

                    ea.Email = GetRecipientEmail(recipient);
                    ea.DisplayName = recipient.Name;
                    listOfAtendees.Add(ea);
                }
                ev.Attendees = listOfAtendees;
            }

            L.O.G("Google event prepared",0);
            return ev;
        }

        public String GetRecipientEmail(Recipient recipient)
        {
            String retEmail;
            if (recipient.AddressEntry.Type == "EX")
            {
                //Exchange
                if (recipient.AddressEntry.AddressEntryUserType == OlAddressEntryUserType.olExchangeUserAddressEntry ||
                    recipient.AddressEntry.AddressEntryUserType == OlAddressEntryUserType.olExchangeRemoteUserAddressEntry)
                {
                    ExchangeUser eu = recipient.AddressEntry.GetExchangeUser();
                    if (eu != null && eu.PrimarySmtpAddress != null)
                        retEmail = eu.PrimarySmtpAddress;
                    else
                    {
                        L.O.G("Exchange does not have an email for this recipient's account!",0);
                        try
                        {
                            PropertyAccessor pa = recipient.PropertyAccessor;
                            retEmail = pa.GetProperty(PR_SMTP_ADDRESS).ToString();
                            L.O.G("Retrieved from PropertyAccessor instead.",0);
                        }
                        catch
                        {
                            L.O.G("Also failed to retrieve email from PropertyAccessor.",0);
                            String buildFakeEmail = recipient.Name.Replace(",", "");
                            buildFakeEmail = buildFakeEmail.Replace(" ", "");
                            buildFakeEmail += "@unknownemail.com";
                            L.O.G("Built a fake email for them: " + buildFakeEmail,0);
                            retEmail = buildFakeEmail;
                        }
                    }
                }
                else
                {
                    PropertyAccessor pa = recipient.PropertyAccessor;
                    retEmail = pa.GetProperty(PR_SMTP_ADDRESS).ToString();
                }
            }
            else
            {
                retEmail = recipient.AddressEntry.Address;
            }
            return retEmail;
        }

        public string SplitAttendees(string attendees)
        {
            if (attendees == null) return "";
            string[] tmp1 = attendees.Split(';');
            for (int i = 0; i < tmp1.Length; i++) tmp1[i] = tmp1[i].Trim();
            return String.Join(Environment.NewLine, tmp1);
        }

        public void AddUserProperty(_AppointmentItem ai, string text, string property)
        {
            UserProperties aiUserProperties = null;
            UserProperty aiUserProperty = null;
            try
            {
                aiUserProperties = ai.UserProperties;
                aiUserProperty = aiUserProperties.Add(property, OlUserPropertyType.olText, true, 1);
                aiUserProperty.Value = text;
                ai.Save();
            }
            catch (Exception ex)
            {
                L.O.G("AddUserProperty error: " + ex.Message,2);
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            finally
            {
                if (aiUserProperty != null) Marshal.ReleaseComObject(aiUserProperty);
                if (aiUserProperties != null) Marshal.ReleaseComObject(aiUserProperties);
            }
        }


        public string GetUserProperty(_AppointmentItem ai, string property)
        {
            string text = "";

            UserProperties aiUserProperties = null;
            UserProperty aiUserProperty = null;
            try
            {
                aiUserProperties = ai.UserProperties;
                aiUserProperty = aiUserProperties.Add(property, OlUserPropertyType.olText, true, 1);
                aiUserProperty = aiUserProperties.Find(property);

                text = aiUserProperty.Value;
            }
            catch (Exception ex)
            {
                L.O.G("GetUserProperty error: " + ex.Message,2);
                //System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            finally
            {
                if (aiUserProperty != null) Marshal.ReleaseComObject(aiUserProperty);
                if (aiUserProperties != null) Marshal.ReleaseComObject(aiUserProperties);
            }

            return text;
        }


        private void ThisAddIn_Shutdown(object sender, EventArgs e)
        {
            SyncTimer.Stop();
        }


        private static void GetVersionInfo()
        {
            try
            {
                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    Assembly addinAssembly = Assembly.GetExecutingAssembly();
                    string cachePath = addinAssembly.CodeBase.Substring(0, addinAssembly.CodeBase.Length -
                                                                           Path.GetFileName(addinAssembly.CodeBase).Length);

                    ApplicationDeployment currentDep = ApplicationDeployment.CurrentDeployment;
                    string deploymentFullName = currentDep.UpdatedApplicationFullName;
                    var appId = new ApplicationIdentity(deploymentFullName);
                    var everything = new PermissionSet(PermissionState.Unrestricted);

                    var trust = new ApplicationTrust(appId);
                    trust.DefaultGrantSet = new PolicyStatement(everything);
                    trust.IsApplicationTrustedToRun = true;
                    trust.Persist = true;

                    ApplicationSecurityManager.UserApplicationTrusts.Add(trust);

                    Properties.Settings.Default.VersionCurrent = currentDep.CurrentVersion.ToString();
                    Properties.Settings.Default.DeployPath = currentDep.UpdateLocation.AbsoluteUri;

                    if (currentDep.CheckForUpdate())
                    {
                        UpdateCheckInfo updateInfo = currentDep.CheckForDetailedUpdate();
                        Properties.Settings.Default.VersionAvailable = updateInfo.AvailableVersion.ToString();

                        if (Properties.Settings.Default.VersionAvailable != Properties.Settings.Default.VersionCurrent)
                        {
                            var ni = new NotifyIcon();
                            ni.Icon = SystemIcons.Question;
                            ni.Visible = true;
                            ni.BalloonTipClicked += notifyIcon1_Click;
                            ni.ShowBalloonTip(5000, "CalendarSync Update", "K dispozici je nová verze " + Properties.Settings.Default.VersionAvailable, ToolTipIcon.Info);
                        }
                    }
                }
            }
            catch (Exception)
            {
                //logboxout("GetVersionInfo error: " + ex.Message);
            }
        }


        public static void GetCurrentVersion()
        {
            try
            {
                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    Assembly addinAssembly = Assembly.GetExecutingAssembly();
                    string cachePath = addinAssembly.CodeBase.Substring(0, addinAssembly.CodeBase.Length -
                                                                           Path.GetFileName(addinAssembly.CodeBase).Length);

                    ApplicationDeployment currentDep = ApplicationDeployment.CurrentDeployment;
                    string deploymentFullName = currentDep.UpdatedApplicationFullName;
                    var appID = new ApplicationIdentity(deploymentFullName);
                    var everything = new PermissionSet(PermissionState.Unrestricted);

                    var trust = new ApplicationTrust(appID);
                    trust.DefaultGrantSet = new PolicyStatement(everything);
                    trust.IsApplicationTrustedToRun = true;
                    trust.Persist = true;

                    ApplicationSecurityManager.UserApplicationTrusts.Add(trust);

                    Properties.Settings.Default.VersionCurrent = currentDep.CurrentVersion.ToString();
                    Properties.Settings.Default.DeployPath = currentDep.UpdateLocation.AbsoluteUri;
                }
            }
            catch (Exception)
            {
            }
        }


        private static void DoUpdate()
        {
            var InstallerPath = new Uri("C:\\Program Files\\Common Files\\Microsoft Shared\\VSTO\\9.0\\VSTOINSTALLER.exe");

            if (Directory.Exists(@"C:\Program Files"))
            {
                try
                {
                    installer64 = Directory.GetFiles(@"C:\Program Files\Common Files\Microsoft Shared\VSTO", "VSTOInstaller.exe", SearchOption.AllDirectories);
                    if (installer64[0].Length > 10) InstallerPath = new Uri(installer64[0]);
                }
                catch (Exception)
                {
                }
            }

            if (Directory.Exists(@"C:\Program Files (x86)"))
            {
                try
                {
                    installer32 = Directory.GetFiles(@"C:\Program Files (x86)\Common Files\Microsoft Shared\VSTO", "VSTOInstaller.exe", SearchOption.AllDirectories);
                    if (installer32[0].Length > 10) InstallerPath = new Uri(installer32[0]);
                }
                catch (Exception)
                {
                }
            }

            try
            {
                var VstoInstallerProc = new Process();
                VstoInstallerProc.StartInfo.Arguments = " /I " + Properties.Settings.Default.DeployPath;
                VstoInstallerProc.StartInfo.FileName = InstallerPath.AbsoluteUri;
                VstoInstallerProc.Start();

                var ni0 = new NotifyIcon();
                ni0.Icon = SystemIcons.Information;
                ni0.Visible = true;
                ni0.ShowBalloonTip(3000, "CalendarSync Update", "Probíhá aktualizace. Prosím vyčkejte...", ToolTipIcon.Info);

                VstoInstallerProc.WaitForExit();

                if (VstoInstallerProc.ExitCode == 0)
                {
                    var ni = new NotifyIcon();
                    ni.Icon = SystemIcons.Information;
                    ni.Visible = true;
                    ni.ShowBalloonTip(3000, "CalendarSync Update", "Aktualizace proběhla v pořádku. Naintalována verze " + Properties.Settings.Default.VersionAvailable + ". Restartujte prosím Outlook.", ToolTipIcon.Info);
                }
                else
                {
                    var ni = new NotifyIcon();
                    ni.Icon = SystemIcons.Error;
                    ni.Visible = true;
                    ni.ShowBalloonTip(3000, "CalendarSync Update", "Vyskytla se chyba při aktualizaci (" + VstoInstallerProc.ExitCode + ")", ToolTipIcon.Error);
                }
            }
            catch (Exception)
            {
            }
        }

        public static void notifyIcon1_Click(object Sender, EventArgs e)
        {
            DoUpdate();
        }


        public static bool isEmail(string inputEmail)
        {
            inputEmail = inputEmail ?? string.Empty;
            string strRegex = @"^(([a-zA-Z0-9_\-\+/\^]+)([\.]?)([a-zA-Z0-9_\-\+/\^]+))+@((\[[0-9]{1,3}" +
                              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            var re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            return (false);
        }


        public bool IsEmail(string emailaddress)
        {
            try
            {
                var m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }


        private String allDayEvent(DateTime time)
        {
            var t = new StringBuilder(";VALUE=DATE:");
            t.Append(time.Year.ToString("0000"));
            t.Append(time.Month.ToString("00"));
            t.Append(time.Day.ToString("00"));

            return t.ToString();
        }


        private String time2ISO(DateTime time)
        {
            var t = new StringBuilder(time.Year.ToString("0000"));
            t.Append(time.Month.ToString("00"));
            t.Append(time.Day.ToString("00"));
            t.Append("T");
            t.Append(time.Hour.ToString("00"));
            t.Append(time.Minute.ToString("00"));
            t.Append(time.Second.ToString("00"));

            return t.ToString();
        }

        private String rruleGetFrequency(RecurrencePattern pattern)
        {
            var f = new StringBuilder();

            switch (pattern.RecurrenceType)
            {
                case OlRecurrenceType.olRecursDaily:
                    f.Append("DAILY");
                    f.Append(";INTERVAL=" + (pattern.Interval.ToString() == "0" ? "1" : pattern.Interval.ToString()));
                    // correct an error in outlook that sets interval to 0 for
                    // daily on all weekdays
                    break;
                case OlRecurrenceType.olRecursWeekly:
                    f.Append("WEEKLY");
                    f.Append(";BYDAY=" + daysOfWeek(pattern.DayOfWeekMask));
                    f.Append(";INTERVAL=" + (pattern.Interval.ToString() == "0" ? "1" : pattern.Interval.ToString()));
                    break;
                case OlRecurrenceType.olRecursMonthly:
                    f.Append("MONTHLY");
                    f.Append(";INTERVAL=" + (pattern.Interval.ToString() == "0" ? "1" : pattern.Interval.ToString()));
                    break;
                case OlRecurrenceType.olRecursMonthNth:
                    f.Append("MONTHLY");
                    f.Append(";BYDAY=" + pattern.Instance + daysOfWeek(pattern.DayOfWeekMask));
                    f.Append(";INTERVAL=" + (pattern.Interval.ToString() == "0" ? "1" : pattern.Interval.ToString()));
                    break;
                case OlRecurrenceType.olRecursYearly:
                    f.Append("YEARLY");
                    f.Append(";INTERVAL=1"); // this catches another error in outlook interval is 12 in yearly events
                    break;
                case OlRecurrenceType.olRecursYearNth:
                    f.Append("YEARLY");
                    f.Append(";BYMONTH=" + pattern.MonthOfYear);
                    f.Append(";BYDAY=" + pattern.Instance + daysOfWeek(pattern.DayOfWeekMask));
                    f.Append(";INTERVAL=1"); // this catches another error in outlook interval is 12 in yearly events
                    break;
                default:
                    // this should never happen, ERROR
                    L.O.G("OLCalItem::get_RRULE_FREQ: Recurrence not implemented",2);
                    //throw new System.Exception("OLCalItem::get_RRULE_FREQ: Recurrence not implemented");
                    break;
            }
            return f.ToString();
        }


        private String daysOfWeek(OlDaysOfWeek dow)
        {
            var d = new StringBuilder();

            if (((int) dow & (int) OlDaysOfWeek.olMonday) == (int) OlDaysOfWeek.olMonday)
            {
                d.Append("MO");
            }

            if (((int) dow & (int) OlDaysOfWeek.olTuesday) == (int) OlDaysOfWeek.olTuesday)
            {
                if (d.Length > 0) d.Append(",");
                d.Append("TU");
            }

            if (((int) dow & (int) OlDaysOfWeek.olWednesday) == (int) OlDaysOfWeek.olWednesday)
            {
                if (d.Length > 0) d.Append(",");
                d.Append("WE");
            }

            if (((int) dow & (int) OlDaysOfWeek.olThursday) == (int) OlDaysOfWeek.olThursday)
            {
                if (d.Length > 0) d.Append(",");
                d.Append("TH");
            }

            if (((int) dow & (int) OlDaysOfWeek.olFriday) == (int) OlDaysOfWeek.olFriday)
            {
                if (d.Length > 0) d.Append(",");
                d.Append("FR");
            }

            if (((int) dow & (int) OlDaysOfWeek.olSaturday) == (int) OlDaysOfWeek.olSaturday)
            {
                if (d.Length > 0) d.Append(",");
                d.Append("SA");
            }

            if (((int) dow & (int) OlDaysOfWeek.olSunday) == (int) OlDaysOfWeek.olSunday)
            {
                if (d.Length > 0) d.Append(",");
                d.Append("SU");
            }

            return d.ToString();
        }

        #region VSTO generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            Startup += ThisAddIn_Startup;
            Shutdown += ThisAddIn_Shutdown;
        }

        #endregion
    }


    public class MyCalendarListEntry
    {
        public string Id = "";
        public string Name = "";


        public MyCalendarListEntry()
        {
        }

        public MyCalendarListEntry(CalendarListEntry init)
        {
            Id = init.Id;
            Name = init.Summary;
        }

        public override string ToString()
        {
            return Name;
        }
    }


    public class CalSettings
    {
        private static CalSettings instance;
        public bool AddAttendeesToDescription = true;

        public bool AddDescription = true;
        public bool AddReminders = false;
        public bool CreateTextFiles = true;
        public int DaysInTheFuture = 60;
        public int DaysInThePast = 1;
        public bool MinimizeToTray = false;
        public string MinuteOffsets = "";
        public string RefreshToken = "";
        public bool ShowBubbleTooltipWhenSyncing = false;
        public bool StartInTray = false;
        public bool SyncEveryHour = false;
        public MyCalendarListEntry UseGoogleCalendar = new MyCalendarListEntry();

        public static CalSettings Instance
        {
            get
            {
                if (instance == null) instance = new CalSettings();
                return instance;
            }
            set { instance = value; }
        }
    }


    public class XMLManager
    {
        /// <summary>
        ///     Exports any object given in "obj" to an xml file given in "filename"
        /// </summary>
        /// <param name="obj">The object that is to be serialized/exported to XML.</param>
        /// <param name="filename">The filename of the xml file to be written.</param>
        public static void export(Object obj, string filename)
        {
            var writer = new XmlTextWriter(filename, null);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 4;
            new XmlSerializer(obj.GetType()).Serialize(writer, obj);
            writer.Close();
        }

        /// <summary>
        ///     Imports from XML and returns the resulting object of type T.
        /// </summary>
        /// <param name="filename">The XML file from which to import.</param>
        /// <returns></returns>
        public static T import<T>(string filename)
        {
            var fs = new FileStream(filename, FileMode.Open);
            var result = (T) new XmlSerializer(typeof (T)).Deserialize(fs);
            fs.Close();
            return result;
        }
    }
}