﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;

namespace CalendarSync
{
    public partial class Ribbon1
    {
        private static Ribbon1 instance;
        public static Ribbon1 Instance
        {
            get
            {
                if (instance == null) instance = new Ribbon1();
                return instance;
            }
        }

        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            //Globals.ThisAddIn.OnSyncButton();
            Globals.ThisAddIn.DoSync();
        }

        private void button_settings_Click(object sender, RibbonControlEventArgs e)
        {
            Settings st1 = new Settings();
            st1.Show();
        }

        public void EnableDisableSyncButton(bool enabled)
        {
            button1.Enabled = enabled;
        }
    }
}
