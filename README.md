### What is this for? ###

* Copy calendar appointments from Outlook to Google Calendar
* Outlook add-in. Tested in 2013.


### How do I get set up? ###

* Using setup.exe is recommended at least for initial installation - it handles dependencies.
* Add-In will update itself automatically.
* .NET4 is required

### How do I use it? ###

* For first time you need to create sync token - go to Settings and click at Get Calendars.
* You can use ad-hoc sync or auto sync - see settings